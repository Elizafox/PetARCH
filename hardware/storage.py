# Hardware routines for petarch
# Licensed under the WTFPLv2.

from os import truncate
from mmap import mmap

from . import Hardware


class StorageController(Hardware):
    """A basic 8-word block storage controller."""

    STORAGE_SIZE = 2048
    DEFAULT_PATH = "storage/storage.bin"

    REGION_SIZE = 10

    def __init__(self, memory, bus, machine, offset, file=DEFAULT_PATH):
        super().__init__(memory, bus, machine, offset)

        # Persistent disk image
        self._mem_f = open(file, "rb+")
        truncate(self._mem_f.fileno(), self.STORAGE_SIZE)
        self.persistent = mmap(self._mem_f.fileno(), 0)

        self.store_block = [0] * 8
        self.cur_block = 0

    def shutdown(self):
        self.persistent.flush()
        self.persistent.close()
        self._mem_f.close()

    @property
    def bsel_offset(self):
        return self.offset

    @property
    def bctrl_offset(self):
        return self.offset + 1

    @property
    def block_offset(self):
        return self.offset + 2

    @property
    def block_offset_max(self):
        # 0 through 7, so 8 items
        return self.block_offset + 10

    # Constants for the block controller
    BCTRL_FLUSH = 0
    BCTRL_READ = 1
    BCTRL_WRITE = 2
    BCTRL_ZERO = 3
    BCTRL_SYNC = 4

    def op_flush(self):
        self.store_block = [0] * 8

    def op_read(self):
        off = self.cur_block * 8
        self.store_block = [b for b in self.persistent[off:off+8]]

    def op_write(self):
        off = self.cur_block * 8
        for i in range(0, 8):
            self.persistent[off+i] = self.store_block[i]

    def op_zero(self):
        off = self.cur_block * 8
        for i in range(0, 8):
            self.persistent[off+i] = 0

        self.store_block = [0] * 8

    def op_sync(self):
        self.persistent.flush()

    FN_OPS = {BCTRL_FLUSH: op_flush,
              BCTRL_READ: op_read,
              BCTRL_WRITE: op_write,
              BCTRL_ZERO: op_zero,
              BCTRL_SYNC: op_sync}

    def __getitem__(self, item):
        if isinstance(item, slice):
            return super().__getitem__(item)

        if item == self.bsel_offset:
            # Storage controller block select register
            return self.cur_block
        elif item >= self.block_offset and item <= self.block_offset_max:
            item -= self.offset
            return self.store_block[item]

        return 0

    def __setitem__(self, item, value):
        if isinstance(item, slice):
            return super().__setitem__(item, value)

        if item == self.bsel_offset:
            # Storage controller block select register
            # Clamped to current storage size.
            self.cur_block = value % (self.STORAGE_SIZE // 8)
        elif item == self.bctrl_offset:
            if value not in self.FN_OPS:
                return

            self.FN_OPS[value](self)
        elif item >= self.block_offset and item <= self.block_offset_max:
            # Storage controller block registers
            item -= self.offset
            value %= 256  # Clamp
            self.store_block[item] = value
