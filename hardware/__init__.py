# Hardware routines for petarch
# Licensed under the WTFPLv2.

from functools import partial
from itertools import zip_longest
from collections import defaultdict


class Hardware:
    """The base class for hardware."""

    REGION_SIZE = -1
    """Size of the memory region this hardware takes."""

    def __init__(self, memory, bus, machine, offset):
        self.memory = memory
        self.bus = bus
        self.machine = machine
        self.offset = offset

    def shutdown(self):
        pass

    def __getitem__(self, item):
        if isinstance(item, slice):
            indices = item.indices(len(self))
            return [self[i] for i in range(*indices)]

        return 0

    def __setitem__(self, item, value):
        if isinstance(item, slice):
            indices = item.indices(len(self))

            len_s = (indices[1] - indices[0]) // indices[2]
            len_v = len(value)

            if len_s > len_v and indices[2] != 1:
                e = ("attempt to assign sequence of size {} to extended "
                     "slice of size {}").format(len_v, len_s)
                raise ValueError(e)

            for (vi, v), i in zip_longest(enumerate(value), range(*indices)):
                if i is None:
                    i = vi + indices[1]

                self[i] = v

    def __len__(self):
        return self.REGION_SIZE


class HardwarePIC(Hardware):
    """Interface for a PIC"""

    def __init__(self, memory, bus, machine, offset):
        super().__init__(memory, bus, machine, offset)
        bus.register_pic(self)

    def signal_interrupt(self, number):
        raise NotImplementedError

    def mask_interrupt(self, number):
        raise NotImplementedError

    def unmask_interrupt(self, number):
        raise NotImplementedError


class HardwareBus:
    """The bus for PetARCH.

    Hardware is attached to the bus and accessed via MMIO.

    All hardware has access to all RAM.

    A PIC can be registered, which allows for interrupts to be generated.
    Interrupt numbers will be allocated on behalf of the PIC. Note that
    routines calling interrupts will raise errors without a PIC registered.
    """

    def __init__(self, memory, machine, hardware):
        total_size = 0
        hardware_table = []
        mapping = []

        self.interrupts = defaultdict(list)
        self.pic = None

        for hw in hardware:
            # total_size is present offset
            instance = hw(memory, self, machine, total_size)
            total_size += instance.REGION_SIZE
            mapping.extend([instance] * instance.REGION_SIZE)
            hardware_table.append(instance)

        self.total_size = total_size
        self.hardware_table = hardware_table
        self.mapping = mapping

        self.memory = memory

    def close(self):
        if not hasattr(self, "hardware_table"):
            # Early failure, bail without cluttering the traceback
            return

        for hw in self.hardware_table:
            hw.shutdown()

    def __del__(self):
        self.close()

    def __getitem__(self, item):
        if isinstance(item, slice):
            indices = item.indices(self.total_size)
            return [self[i] for i in range(*indices)]
        else:
            # Hardware mapping -> hardware itself
            return self.mapping[item][item]

    def __setitem__(self, item, value):
        if isinstance(item, slice):
            indices = item.indices(len(self))

            len_s = (indices[1] - indices[0]) // indices[2]
            len_v = len(value)

            if len_s > len_v and indices[2] != 1:
                e = ("attempt to assign sequence of size {} to extended "
                     "slice of size {}").format(len_v, len_s)
                raise ValueError(e)

            for (vi, v), i in zip_longest(enumerate(value), range(*indices)):
                if i is None:
                    i = vi + indices[1]

                self[i] = v
        else:
            self.mapping[item][item] = value

    def __len__(self):
        return self.total_size

    def register_pic(self, pic):
        self.pic = pic

    def register_interrupt(self, hardware, interrupt):
        if self.pic is None:
            raise Exception("No PIC present")

        self.interrupts[interrupt].append(hardware)

    def signal_interrupt(self, number):
        self.pic.signal_interrupt(number)

    def mask_interrupt(self, number):
        self.pic.mask_interrupt(number)

    def unmask_interrupt(self, number):
        self.pic.unmask_interrupt(number)

    def interrupt_enable(self):
        self.pic.gmask = False

    def interrupt_disable(self):
        self.pic.gmask = True


# Import hardware for the default mapper
from .printer import Printer
from .power import PowerController
from .dummy import Dummy
from .storage import StorageController
from .sound import BeeperController
from .pic import PIC
from .timer import RTC
from .pnp import PNP


class DefaultHardwareBus(HardwareBus):
    """The default hardware mapping class"""

    # Dummy regions are for backwards/forwards compat
    HARDWARE = [Printer, PowerController, PIC, StorageController,
                BeeperController, RTC, partial(Dummy, size=1998), PNP]

    def __init__(self, memory, machine):
        super().__init__(memory, machine, self.HARDWARE)
