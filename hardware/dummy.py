# Hardware routines for petarch
# Licensed under the WTFPLv2.

from . import Hardware


class Dummy(Hardware):
    """Empty region."""

    def __init__(self, memory, bus, machine, offset, size):
        super().__init__(memory, bus, machine, offset)
        self.REGION_SIZE = size
