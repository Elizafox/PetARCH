# Hardware routines for petarch
# Licensed under the WTFPLv2.

from . import Hardware


class PNP(Hardware):
    """A basic PNP system.
    
    Basically, give the PNP controller somewhere to write information, and
    it will tell you everything about a given piece of hardware.

    For interrupt info: you need at least 259 bytes available to write to.
    For index info: you need 256 bytes available.
    """

    REGION_SIZE = 12

    OFF_VERB = 0
    """Action to perform"""

    OFF_HW_INFO = 1
    """Interrupt/hardware ID to probe"""

    OFF_HW_INDEX = 2
    """For pagination of interrupt info"""

    OFF_ADDR_START = 3
    """Start of address space for read/write"""

    OFF_ADDR_END = 11
    """End of address space for read/write"""

    # Verbs
    CTRL_INT_INFO = 1
    """Dump info on interrupt at address space given"""

    CTRL_INDEX_INFO = 2
    """Dump info on hardware index at address space given"""

    def __init__(self, memory, bus, machine, offset):
        super().__init__(memory, bus, machine, offset)

        self.last_cmd = 0
        self.hw_info = 0
        self.hw_index = 0
        self.address = 0

    def dump_info_hardware(self, hw_id, offset):
        bus = self.bus

        # byte 0: hardware present
        if hw_id not in bus.hardware_table:
            self.memory[offset] = 0
            return

        hw = bus.hardware_table[hw_id]

        self.memory[offset] = 1
        offset += 1

        # byte 1: active (for now, true)
        self.memory[offset] = 1
        offset += 1

        # byte 2: device attachment id
        self.memory[offset] = hw_id
        offset += 1

        # bytes 3-6: vendor ID (DEADBEEF)
        # (length: 4 bytes)
        self.memory[offset:offset+4] = b"\xDE\xAD\xBE\xEF"
        offset += 4

        # bytes 7-10: hardware ID (F00FCAFE for now)
        # (length: 4 bytes)
        self.memory[offset:offset+4] = b"\xF0\x0F\xCA\xFE"
        offset += 4

        # byte 11: device function
        hwname = hw.__class__.__name__
        self.memory[offset] = hash(hwname) & 0xFF  # Cheesy hack
        offset += 1

        # byte 12: attachment interface (default 0)
        self.memory[offset] = 0
        offset += 1

        # bytes 13-45: vendor name, NULL-terminated
        # (length: 32 bytes)
        vendor = "MearfCo"
        data = vendor.encode()[:31]
        data += b"\0"
        self.memory[offset:len(data)] = data
        offset += 32

        # bytes 46-78: hardware name, NULL terminated
        # (length: 32 bytes)
        data = hwname.encode()[:31]
        data += b"\0"
        self.memory[offset:len(data)] = data
        offset += 32

        # bytes 79-127: reserved
    
    def dump_info_interrupt(self, interrupt, offset):
        bus = self.bus

        if interrupt not in bus.interrupts:
            # Interrupt not in use
            self.memory[offset:offset+1] = b"\x00\x00"
            return

        # Find hardware
        interrupt_list = bus.interrupts[interrupt]
        interrupt_list_len = len(interrupt_list)
        if self.hw_index >= interrupt_list_len:
            # Exceeded the index
            self.memory[offset] = 0
            self.memory[offset+1] = interrupt_list_len
            return

        hardware = interrupt_list[self.hw_index]
        hw_id = bus.hardware_table.index(hardware)

        # byte 0: interrupt (or interrupt slot) in use
        self.memory[offset] = 1
        offset += 1

        # byte 1: number of devices on this interrupt
        self.memory[offset] = interrupt_list_len
        offset += 1

        # All further bytes: hardware info
        self.dump_hardware_info(hw_id, offset)

    def __getitem__(self, item, value):
        if isinstance(item, slice):
            return super().__getitem__(item, value)

        if item == self.OFF_VERB:
            return self.last_cmd
        elif item == self.OFF_HW_INFO:
            return self.hw_info
        elif item == self.OFF_HW_INDEX:
            return self.hw_index
        elif item >= self.OFF_ADDR_START or item <= self.OFF_ADDR_END:
            shift = (item - self.OFF_ADDR_START) << 3
            return (self.address >> shift) & 0xFF
        else:
            return 0

    def __setitem__(self, item, value):
        if isinstance(item, slice):
            return super().__setitem__(item, value)

        bus = self.bus

        if item == self.OFF_VERB:
            self.last_cmd = item
            if value == self.CTRL_INT_INFO:
                # Dump interrupt info
                self.dump_interrupt_info(self.hw_info)
            elif value == self.CTRL_INDEX_INFO:
                self.dump_hardware_info(self.hw_info)
            else:
                self.last_cmd = 0
                return
        elif item == self.OFF_HW_INFO:
            self.hw_info = value
        elif item == self.OFF_HW_INDEX:
            self.hw_index = value
        elif item >= self.OFF_ADDR_START or item <= self.OFF_ADDR_END:
            shift = (item - self.OFF_ADDR_START) << 3
            mask = 0xFF << shift
            value <<= shift
            self.address = (self.address & ~mask) | (value & mask)
        else:
            return
