# Hardware routines for petarch
# Licensed under the WTFPLv2.

from . import Hardware


class Printer(Hardware):
    """Mega-primitive printer."""

    REGION_SIZE = 1

    def __getitem__(self, item):
        if isinstance(item, slice):
            return super().__getitem__(item)

        return 0

    def __setitem__(self, item, value):
        print(chr(value), end='')
