# Hardware routines for petarch
# Licensed under the WTFPLv2.

from . import Hardware

class PowerController(Hardware):
    """Control machine power"""

    REGION_SIZE = 1

    def __getitem__(self, item):
        if isinstance(item, slice):
            return super().__getitem__(item)

        return 0

    def __setitem__(self, item, value):
        if isinstance(item, slice):
            return super().__setitem__(item, value)

        if item == 1:
            print()
            print("Shutting down!")
            quit()
        elif item == 2:
            print("Resetting CPU!")
            self.machine.reset_cpu()
