# Hardware routines for petarch
# Licensed under the WTFPLv2.

from time import time, monotonic

from . import Hardware


class RTC(Hardware):
    """Simple RTC that uses Unix time."""

    REGION_SIZE = 10
    RTC_SIZE = 8  # size of RTC time

    OFF_INT_ENABLE = RTC_SIZE  # Enable timer interrupt
    OFF_FREQ = RTC_SIZE + 1  # Frequency

    INT_NUM = 1

    def __init__(self, memory, bus, machine, offset):
        super().__init__(memory, bus, machine, offset)

        # Register our interrupt
        self.bus.register_interrupt(self, self.INT_NUM)

        self.start = round(monotonic())  # Starting point for when RTC is set
        self.rtc_start = round(time())

        self.int_task = None
        self.int_enable = False
        self.off_freq = 0

    def schedule_rtc_interrupt(self, rescheduling=False):
        if self.int_task and not rescheduling:
            self.machine.sched.cancel(self.int_task)
            self.int_task = None

        if self.int_enable and self.off_freq > 0:
            # Start the event
            self.int_task = self.machine.enter(self.off_freq,
                                               self.rtc_interrupt)

    def rtc_interrupt(self):
        self.bus.signal_interrupt(self.INT_NUM)
        self.reschedule_rtc_interrupt(True)

    def get_rtc_time(self):
        current_time = (round(monotonic()) - self.start) + self.rtc_start
        print(current_time)
        return current_time

    def __getitem__(self, item):
        if item < self.RTC_SIZE:
            return (self.get_rtc_time() >> (item << 3)) & 0xFF
        elif item == self.OFF_INT_ENABLE:
            return int(self.int_enable)
        elif item == self.OFF_FREQ:
            return int(self.off_freq)

        return 0

    def __setitem__(self, item, value):
        if isinstance(item, slice):
            return super().__setitem__(item, value)

        if item < self.RTC_SIZE:
            # Reset RTC offset
            shift = item << 3
            mask = 0xFF << shift
            value <<= shift
            self.rtc_start = (self.rtc_start & ~mask) | (value & mask)

            # Restart clock starting point
            self.start = round(monotonic())
        elif item == self.OFF_INT_ENABLE:
            self.int_enable = value != 0
            self.schedule_rtc_interrupt()
        elif item == self.OFF_FREQ:
            self.off_freq = value
            self.schedule.rtc_interrupt()
