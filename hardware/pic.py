# Hardware routines for petarch
# Licensed under the WTFPLv2.

from . import HardwarePIC


class Interrupt:
    """Data for an interrupt"""

    __slots__ = ["triggered", "masked"]

    def __init__(self):
        self.triggered = False
        self.masked = False


class PIC(HardwarePIC):
    """PetARCH only has one interrupt pin by design, similar to PPC.

    So, it's "wired" up to this thing.

    You get 256 interrupts, It's up to you to write a handler that interacts
    with the PIC.

    Only one NMI: interrupt 0, which signals PIC faults.

    Upon interrupt reciept, all interrupts are disabled. It's up to you to
    reenable them.

    The PIC starts up all interrupts masked.
    """

    REGION_SIZE = 13

    MAX_INTERRUPTS = 256

    OFF_INUM = 0
    """Interrupt number being interacted with"""

    OFF_VERB = 1
    """Action to perform on interrupt"""

    OFF_RESULTS = 2
    """Results of a query"""

    OFF_GMASK = 3
    """Global interrupt mask on/off"""

    OFF_ERROR = 4
    """Error with PIC"""

    OFF_DATA = 5
    """Data for interrupt query (8 bytes)"""

    # Verbs
    CTRL_NONE = 0
    """No action"""

    CTRL_MASK = 1
    """Mask interrupt"""

    CTRL_UNMASK = 2
    """Unmask interrupt"""

    CTRL_SERVICED = 3
    """Set interrupt to serviced"""

    CTRL_QUERY = 4
    """Query interrupt status"""

    CTRL_TRIGGER = 5
    """Trigger interrupt"""

    CTRL_REPORT = 6
    """Dump interrupt table at location pointed to by OFF_DATA"""

    # Errors
    ERR_NONE = 0
    """No error."""

    ERR_NUM = 1
    """Invalid interrupt number"""

    ERR_UNMASKABLE = 2
    """Interrupt cannot be masked"""

    ERR_DOOFUS = 3
    """Manually called an interrupt that can't be triggered."""

    ERR_BADVERB = 4
    """Invalid verb"""

    def __init__(self, memory, bus, machine, offset):
        super().__init__(memory, bus, machine, offset)

        self.itable = [Interrupt() for _ in range(self.MAX_INTERRUPTS)]

        self.inum = 0
        self.results = 0
        self.data = 0
        self.verb = self.CTRL_NONE
        self.gmask = False
        self.error = self.ERR_NONE

    def __getitem__(self, item):
        if isinstance(item, slice):
            return super().__getitem__(item)

        if item == self.OFF_INUM:
            return self.inum
        elif item == self.OFF_VERB:
            return self.verb
        elif item == self.OFF_RESULTS:
            return self.results
        elif item == self.OFF_GMASK:
            return self.gmask
        elif item == self.OFF_ERROR:
            return self.error
        elif item >= self.OFF_DATA:
            shift = (item - self.OFF_DATA) << 3
            return (self.data >> shift) & 0xFF
        else:
            return 0

    def __setitem__(self, item, value):
        if isinstance(item, slice):
            return super().__setitem__(item, value)

        if item == self.OFF_INUM:
            if value >= self.MAX_INTERRUPTS:
                # Out of bounds
                self.error = self.ERR_NUM
                self.signal_interrupt(0)
                return

            self.inum = value
        elif item == self.OFF_VERB:
            self.handle_verb(value)
        elif item == self.OFF_RESULTS:
            self.results = value & 0x3
        elif item == self.OFF_GMASK:
            self.gmask = (value > 0)
        elif item == self.OFF_ERROR:
            self.error = value
        elif item >= self.OFF_DATA:
            shift = (item - self.OFF_DATA) << 3
            mask = 0xFF << shift
            value <<= shift
            self.data = (self.data & ~mask) | (value & mask)

    def signal_interrupt(self, interrupt):
        """Signal an interrupt has taken place"""
        if self.gmask and interrupt != 0:
            # All interrupts masked
            # (except zero, which can't be masked)
            return

        if interrupt >= self.MAX_INTERRUPTS:
            self.error = self.ERR_NUM
            self.signal_interrupt(0)
            return

        interrupt_data = self.itable[interrupt]
        interrupt_data.triggered = True

        # Signal if not masked
        if interrupt_data.masked and interrupt != 0:
            return

        # Set interrupt state
        self.inum = interrupt

        # All interrupts masked
        self.gmask = True

        self.machine.interrupt()

    def mask_interrupt(self, interrupt):
        """Mask an interrupt"""
        if interrupt >= self.MAX_INTERRUPTS:
            self.error = self.ERR_NUM
            self.signal_interrupt(0)
            return
        elif interrupt == 0:
            # Can't mask interrupt 0
            self.error = self.ERR_UNMASKABLE
            self.signal_interrupt(0)
            return

        interrupt_data = self.itable[interrupt]
        interrupt_data.masked = True

    def unmask_interrupt(self, interrupt):
        """Unmask an interrupt"""
        if interrupt >= self.MAX_INTERRUPTS:
            self.error = self.ERR_NUM
            self.signal_interrupt(0)
            return

        interrupt_data = self.itable[interrupt]
        interrupt_data.masked = False

    def handle_verb(self, verb):
        if verb == self.CTRL_NONE:
            return
        elif verb == self.CTRL_MASK:
            self.mask_interrupt(self.inum)
        elif verb == self.CTRL_UNMASK:
            self.unmask_interrupt(self.inum)
        elif verb == self.CTRL_SERVICED:
            self.itable[self.inum].triggered = False
        elif verb == self.CTRL_QUERY:
            self.results = self.itable[self.inum].triggered
            self.results |= (self.itable[self.inum].masked << 1)
        elif verb == self.CTRL_TRIGGER:
            if self.inum == 0:
                # Can't call interrupt 0.
                self.error = self.ERR_DOOFUS
                return

            self.signal_interrupt(self.inum)
        elif verb == self.CTRL_REPORT:
            start = self.data
            end = start + (len(self.itable) - 1)

            write = [d.triggered | (d.masked << 1) for d in self.itable]
            self.memory[start:end] = write
        else:
            self.error = self.ERR_BADVERB
            self.signal_interrupt(0)
