# Hardware routines for petarch
# Licensed under the WTFPLv2.

from os import system

from . import Hardware


class BeeperController(Hardware):
    """Ultra-primitive beeper."""

    REGION_SIZE = 3

    def __init__(self, memory, bus, machine, offset):
        super().__init__(memory, bus, machine, offset)

        self.beeper_duration = 0
        self.beeper_frequency = 0

    @property
    def beep_duration_offset(self):
        return self.offset

    @property
    def beep_frequency_offset(self):
        return self.offset + 1

    @property
    def beep_control_offset(self):
        return self.offset + 2

    def __getitem__(self, item):
        if isinstance(item, slice):
            return super().__getitem__(item)

        if item == self.beep_duration_offset:
            return self.beeper_duration
        elif item == beep_frequency_offset:
            return self.beeper_frequency
        elif item == self.beep_control_offset:
            # TODO asynchronous beep
            return 0

    def __setitem__(self, item, value):
        if isinstance(item, slice):
            return super().__setitem__(item, value)

        if item == self.beep_duration_offset:
            self.beeper_duration = value
        elif item == beep_frequency_offset:
            self.beeper_frequency = value
        elif item == self.beep_control_offset:
            if value > 0:
                system("beep -f {} -l {}".format(self.beep_duration,
                                                 self.beep_frequency))
