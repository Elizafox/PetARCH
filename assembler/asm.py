#!/usr/bin/env python3

# Assembler for PetARCH.
# Licensed under the WTFPLv2.


from collections import defaultdict
from itertools import chain
from sys import argv
from pyparsing import (Word, Group, Regex, LineStart, LineEnd, restOfLine,
                       Optional, oneOf, delimitedList, alphanums, nums)

# Local imports
from ..loader.memoryimage import MemoryImage
from ..machine import Machine


# Arithmetic instructions (3-arg)
instr_arith = ("p", "s", "m", "d", "a", "o", "x", "sr", "sl")

# Jump instructions (2-arg)
instr_jmp = ("je", "jn", "jg", "jl")

# Copy instructions
instr_cp = ("mc", "mr", "mmr", "mrm")

# Two-argument misc instructions
instr_tms = ("i", "f")

# No-argument instructions
instr_na = ("n",)

# Pseudo-instructions
instr_pseudo = ("data", "pad")

# Opcodes
opcodes = {"n": 0, "p": 1, "s": 2, "je": 3, "jn": 4, "jg": 5, "jl": 6, "mc": 7,
           "mr": 8, "mmr": 9, "mrm": 10, "a": 11, "o": 12, "x": 13, "f": 14,
           "sr": 15, "sl": 16, "m": 17, "d": 18, "i": 19, "data": -1,
           "pad": -1}

# Register aliases
register_alias = {"c": 32, "sp": 33, "cp": 34, "rp": 35, "e": 36, "j": 37,
                  "fault": 38, "traps": 39}

# Fault names
faults = {"dvz": 1, "ill": 2, "inv": 4, "mem": "8", "dbl": 16}

# Registers
reg_count = Machine.NUM_REGS

# Lower address space reserved
lower_reserved = 0x100

# Grammar
mnemonics = " ".join(chain(instr_arith, instr_jmp, instr_cp, instr_tms,
                           instr_na, instr_pseudo))

mnemonic = oneOf(mnemonics, caseless=True).setResultsName("mnemonic")
register = Regex(r"\$[A-Za-z0-9]{1,5}").setResultsName("register",
                                                 listAllMatches=True)
constant = Word(nums).setResultsName("constant", listAllMatches=True)
fault = Regex("\#" + "[A-Za-z]+").setResultsName("fault", listAllMatches=True)
label_name = Regex("\!" + "[A-Za-z0-9_]+").setResultsName("label_name")
var_name = Regex("\." + "[A-Za-z0-9_]+").setResultsName("var_name")

argument = (register | constant | label_name | fault |
            var_name).setResultsName("argument")
argument_list = delimitedList(argument).setResultsName("argument_list")

instruction = (mnemonic + Optional(argument_list)).setResultsName(
    "instruction")
label = (label_name + ":").setResultsName("label")
var_def = (var_name + "=" + (constant | label |
                             register)).setResultsName("var_def")

comment = (Word("//") + restOfLine).suppress().setResultsName("comment")

p_line = (instruction | label | var_def | LineEnd()).ignore(comment)


# Assembler state
label_codepos = {}
vars = {}
pending_label = defaultdict(list)
code = []

def mark_label(label, pos):
    """Mark given label."""
    if label in pending_label:
        for res in pending_label[label]:
            code[res] = lower_reserved + pos

        del pending_label[label]
    if label in label_codepos:
        print("Warning: ignoring attempt to redefine label {}".format(label))
        return

    label_codepos[label] = lower_reserved + pos


def resolve_label(label, pos):
    """Resolve the given label, or add position to pending list."""
    if label in label_codepos:
        return label_codepos[label]
    else:
        if pos is not None:
            pending_label[label].append(pos)

        return 0


def to_reg(line, argpos):
    """Turn an argument position into a register."""
    ch = line.argument_list[argpos][0]
    if ch == '$':
        reg = line.argument_list[argpos][1:].lower()
        if reg.isdigit():
            val = int(reg)
        elif reg in register_alias:
            val = register_alias[reg]
        else:
            raise Exception("Argument must be a valid alias or register")
    elif ch == '.':
        val = vars[line.argument_list[argpos]]
    elif ch == '#':
        val = faults.get(line.argument_list[argpos][1:].lower(), None)
        raise Exception("Invalid fault name")
    else:
        # Alors bof...
        raise Exception("Argument must be a label, variable, fault alias, "
                        "or register")

    if val >= reg_count:
        raise Exception("Invalid register.")

    return val


def parse_arith(opcode, line):
    if len(line.argument_list) != 3:
        raise Exception("3 arguments expected, got {}".format(
            line.argument_list))

    arg_0 = to_reg(line, 0)
    arg_1 = to_reg(line, 1)
    arg_2 = to_reg(line, 2)

    return (opcode, arg_0, arg_1, arg_2)


def parse_jmp(opcode, line):
    if len(line.argument_list) != 3:
        raise Exception("3 arguments expected, got {}".format(
            line.argument_list))

    res = tuple()

    arg_0 = to_reg(line, 0)
    arg_1 = to_reg(line, 1)

    # c is the jump point and will either be a label, register, or
    # absolute position.
    if line.argument_list[2][0] == '$':
        # Register
        arg_2 = to_reg(line, 2)
    elif line.constant:
        # Absolute
        arg_2 = int(line.constant[0])
        res += (opcode, arg_0, arg_1, arg_2)
    elif line.var_name:
        # Variable (variant of absolute)
        arg_2 = vars[line.var_name]
    else:
        # Label
        label = line.argument_list[2]
        arg_2 = resolve_label(label, len(code) + 1)

    # No branch immediate, so use reg 31
    res += (opcodes["mc"], arg_2, 31, 0)
    res += (opcode, arg_0, arg_1, 31)
    return res


def parse_cp(opcode, line):
    if len(line.argument_list) != 2:
        raise Exception("2 arguments expected, got {}".format(
            line.argument_list))

    mnemonic = line.mnemonic.lower()

    if mnemonic == "mc":
        # First argument is a constant value or label.
        if not (line.constant or line.label_name or line.var_name):
            raise Exception("Constant, variable, or label expected")

        if line.label_name:
            arg_0 = resolve_label(line.argument_list[0],
                                  len(code) + 2)
        elif line.var_name:
            arg_0 = vars[line.var_name]
        else:
            arg_0 = int(line.constant[0])
    else:
        # All others do not take constant values
        arg_0 = to_reg(line, 0)

    arg_1 = to_reg(line, 1)

    return (opcode, arg_0, arg_1, 0)


def parse_tms(opcode, line):
    # All of these instructions take registers
    if len(line.argument_list) != 2:
        raise Exception("2 arguments expected, got {}".format(
            line.argument_list))

    arg_0 = to_reg(line, 0)
    arg_1 = to_reg(line, 1)

    return (opcode, arg_0, arg_1, 0)


def parse_na(opcode, line):
    # No arguments
    if len(line.argument_list) != 0:
        raise Exception("Zero arguments expected.")

    return (opcode, 0, 0, 0)


def parse_pseudo(line):
    mnemonic = line.mnemonic.lower()
    if mnemonic == "data":
        # All arguments are raw data.
        if len(line.constant) != len(line.argument_list):
            raise Exception("Malformed data")

        return tuple(int(arg) for arg in line.argument_list)
    elif mnemonic == "pad":
        if len(line.constant) != len(line.argument_list) != 2:
            raise Exception("Malformed pad")

        return (int((line.constant[0],)) * int(line.constant[1]))

def parse(line, count):
    if line.var_def:
        if line.label_name:
            if line.label_name not in label_codepos:
                print("Warning: line {}: Variable definitions "
                      "containing labels MUST be after the label "
                      "definition.".format(count))
                print("Disregarding value.")
                return

            vars[line.var_name] = label_codepos[line.label_name]
        elif line.register:
            vars[line.var_name] = int(line.register[0][1:])
        else:
            assert line.constant
            vars[line.var_name] = int(line.constant[0])

        return
    elif line.label:
        label = line.label[0]
        assert label
        assert label not in label_codepos

        mark_label(label, len(code))

        return

    mnemonic = line.mnemonic.lower()
    if not mnemonic:
        return

    opcode = opcodes[mnemonic]

    if mnemonic in instr_arith:
        val = parse_arith(opcode, line)
    elif mnemonic in instr_jmp:
        val = parse_jmp(opcode, line)
    elif mnemonic in instr_cp:
        val = parse_cp(opcode, line)
    elif mnemonic in instr_tms:
        val = parse_tms(opcode, line)
    elif mnemonic in instr_na:
        val = parse_na(opcode, line)
    elif mnemonic in instr_pseudo:
        val = parse_pseudo(line)

    code.extend(val)

lc = 0
try:
    with open(argv[1], "r") as f:
        for text in f.readlines():
            lc += 1
            line = p_line.parseString(text)
            parse(line, lc)
except Exception as e:
    print("Error at", lc)
    print(str(e))
    raise e
    quit()

if pending_label:
    print("Error: label(s) not defined", pending_label.keys())
    print("Known labels:", label_codepos.keys())
    quit()


MemoryImage.write_file(argv[2], code)
