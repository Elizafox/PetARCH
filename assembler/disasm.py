#!/usr/bin/env python3

# Disassembler for PetARCH.
# Licensed under the WTFPLv2.

from sys import argv
from contextlib import closing
from itertools import islice
from os.path import exists, getsize

# Local imports
from ..loader.memoryimage import MemoryImage


# Opcodes
opcodes_rev = {0: 'n', 1: 'p', 2: 's', 3: 'je', 4: 'jn', 5: 'jg', 6: 'jl',
               7: 'mc', 8: 'mr', 9: 'mmr', 10: 'mrm', 11: 'a', 12: 'o',
               13: 'x', 14: 'f', 15: 'sr', 16: 'sl', 17: 'm', 18: 'd',
               19: 'i'}

reg3_fmt = '${0}, ${1}, ${2}'
reg2_fmt = '${0}, ${1}'

opcodes_fmt = {'n': '',
               'p': reg3_fmt,
               's': reg3_fmt,
               'je': reg3_fmt,
               'jn': reg3_fmt,
               'jg': reg3_fmt,
               'jl': reg3_fmt,
               'mc': '{0}, ${1}',
               'mr': reg2_fmt,
               'mmr': reg2_fmt,
               'mrm': reg2_fmt,
               'a': reg3_fmt,
               'o': reg3_fmt,
               'x': reg3_fmt,
               'f': reg2_fmt,
               'sr': reg3_fmt,
               'sl': reg3_fmt,
               'm': reg3_fmt,
               'd': reg3_fmt,
               'i': reg2_fmt,}


if not exists(argv[1]):
    print("No such image.")
    quit(1)
elif getsize(argv[1]) % MemoryImage.CELL_SIZE != 0:
    print("Malformed image.")
    quit(2)

print("// Disassembly of {}".format(argv[1]))
print()

with closing(MemoryImage.open_mmap(argv[1], -1)) as m:
    m_iter = enumerate(zip(*([iter(m)] * 4)))
    for index, (opcode, arg_0, arg_1, arg_2) in m_iter:
        index *= 4

        if opcode in opcodes_rev:
            opcode = opcodes_rev[opcode]
            print(opcode, opcodes_fmt[opcode].format(arg_0, arg_1, arg_2),
                  '\t', "//", index)
        else:
            # "opcode"
            print("data {0}, {1}, {2}, {3}".format(opcode, arg_0, arg_1,
                                                   arg_2),
                  "\t", "//", index)

print()
print("// End of file.")

