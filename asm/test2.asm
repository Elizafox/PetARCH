// Hello world

mc 0, $0
je $0, $0, !start

!text:
data 72, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100, 10, 0, 0, 0, 0

!start:
mc 0, $0	// Zero for comparison
mc 1, $1	// One for loop
mc !text, $2	// String start

mc 0, $10	// Zero offset for I/O
i $10, $10	// I/O offset for print

!print:
mmr $2, $3	// Get string character
je $0, $3, !end	// Finished?

mrm $3, $10	// Print
p $2, $1, $2	// Increment string index
je $0, $0, !print

!end:		// Initiate shutdown
i $1, $10	// I/O offset for shutdown
mrm $1, $10	// Goodbye
