// demo for storage

mc 1, $0         // Let's add some numbers.
mc 1, $1
p $0, $1, $2
p $2, $2, $2     // And add 'em again
mc 48, $0        // ASCII 0 = code point 48
p $2, $0, $0
mc 0, $4
i $4, $4         // Get I/O base address
mrm $0, $4       // Blam!
mc 10, $3
mrm $3, $4       // The Terminator™
mc 0, $3
mc 14, $4
i $4, $4         // I/O controller offset
mrm $3, $4       // Select I/O block 0
p $1, $4, $4
mrm $3, $4       // Clear I/O block registers
p $1, $4, $4
mrm $2, $4       // Block I/O byte 0
s $4, $1, $4
mc 2, $3
mrm $3, $4       // Write block
mc 4, $3
mrm $3, $4       // Synchronise
mc 32, $0        // Prepare wait loop
mc 0, $2
!loop:
n
s $0, $1, $0     // Decrement loop counter
jn $0, $2, !loop // Loop
i $1, $4
mrm $2, $4       // Shutdown
