#!/usr/bin/env python3

# A simple virtual machine implementing PetARCH.
# Licensed under the WTFPLv2.

from contextlib import closing
from os.path import exists
from sys import argv
from enum import IntFlag
from math import ceil
from inspect import getmembers
from functools import wraps
from sched import scheduler
from warnings import warn

# Local imports
from loader.memoryimage import MemoryImage
from hardware import DefaultHardwareBus
from memory import MemoryMap


def opcode(opcode):
    """Wrapper to add opcode attribute to functions"""
    def wrapper(fn):
        fn.opcode = opcode
        return fn
    return wrapper


class Machine:
    """The virtual machine.

    A very simple RISC machine.

    32 GP integer registers, 0-31. Sign bits ignored.

    Separate dedicated carry, stack pointer, code pointer, and return pointer
    registers.

    The list of present traps are stored in a bit field in REG_TENABLE. The
    fault condition is signalled by REG_TRAP, which MUST be reset after
    clearing a trap. The trap handler jump positions are stored in the trap
    table, at 0x0-0x100, in order they appear in REG_TRAP. The addresses are
    the size of the register width.

    All hardware is MMIO mapped, starting at the end of the code (this will
    change in the future).

    PIC is supported through the REG_J register, signalling jump offsets.

    There is one interrupt, handled as a normal trap. Use a PIC to handle more
    than this.

    Register width is customisable, defaults to 32.
    """

    NUM_REGS = 40

    REG_C = 32
    REG_SP = 33
    REG_CP = 34
    REG_RP = 35
    REG_E = 36
    REG_J = 37
    REG_TRAP = 38
    REG_TENABLE = 39

    class Traps(IntFlag):
        # Trap flags
        DVZ = 0x1
        ILL = 0x2
        INV = 0x4
        MEM = 0x8
        DBL = 0x10
        INT = 0x20

    def __init__(self, image, memory_mapper, bus, bits=32):
        self.bits = bits

        # Max register value
        self.rsize = (1 << bits) - 1

        self.cell_size = image.CELL_SIZE

        # Number of cells occupied by a register
        self.cells_per_reg = ceil(bits / image.CELL_SIZE)

        # Register init
        self.reset_cpu()

        # Trap locations in memory
        self.trap_locs = {k: (i * self.cells_per_reg) for i, k in
                          enumerate(self.Traps)}

        # Generate opcode table
        self.opcodes = {fn[1].opcode: fn[1] for fn in
                        getmembers(self, lambda x: hasattr(x, "opcode"))}

        # Set up scheduler
        self.sched = scheduler()

        # Initalise the memory space.
        self.memory = memory_mapper(image, self, bus)

    def dump_regs(self):
        """Print out register dump"""
        print("Registers:")
        for i in range(0, 31):
            reg = self.registers[i]
            print(f"{i}: {reg:08x}")

        print(f"REG_C: {self.registers[self.REG_C]:08x}")
        print(f"REG_SP: {self.registers[self.REG_SP]:08x}")
        print(f"REG_CP: {self.registers[self.REG_CP]:08x}")
        print(f"REG_E: {self.registers[self.REG_E]:08x}")
        print(f"REG_J: {self.registers[self.REG_J]:08x}")
        print(f"REG_TRAP: {self.registers[self.REG_TRAP]:08x}")
        print(f"REG_TENABLE: {self.registers[self.REG_TENABLE]:08x}")

    def reset_cpu(self):
        """Reset the CPU"""
        self.registers = [0] * self.NUM_REGS

        # Default code position
        self.registers[self.REG_CP] = self.registers[self.REG_RP] = 0x100

    def jump(self, point, relative=True):
        """Perform a jump."""
        if relative:
            point += self.registers[self.REG_J]

        self.registers[self.REG_RP] = (self.registers[self.REG_CP] +
                                       self.cells_per_reg)
        self.registers[self.REG_CP] = point

    def maybe_fault(self, t):
        """Call fault handler, if it exists."""
        reg_trap = self.registers[self.REG_TRAP]
        reg_tenable = self.registers[self.REG_TENABLE]

        # Set given fault condition
        self.registers[self.REG_TRAP] |= t

        if reg_trap:
            # Double fault!
            if reg_tenable & self.Traps.DBL:
                if reg_trap & self.Traps.DBL:
                    # DBL set, this means this is the third fault.
                    print("Triple fault!")
                    print("Resetting CPU!")
                    self.dump_regs()
                    self.reset_cpu()
                    return

                # Set double fault status and call the double fault handler
                self.registers[self.REG_TRAP] |= self.Traps.DBL
                jmp_pos = self.memory[self.trap_locs[self.Traps.DBL]]
                self.jump(jmp_pos, False)
                return
            else:
                # Trap handler for double fault disabled
                print("Double fault with double fault handler disabled!")
                print("Resetting CPU!")
                self.dump_regs()
                self.reset_cpu()
                return

        if t & reg_tenable:
            # Jump to fault handler
            jmp_pos = self.memory[self.trap_locs[t]]
            self.jump(jmp_pos, False)

    @opcode(0x00)
    def op_n(self, a, b, c):
        """no-op.

        mnemonic: n
        """

    @opcode(0x01)
    def op_p(self, a, b, c):
        """Add a and b together and store the result in c.

        mnemonic: p (plus)
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l or c > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        r = self.registers[a] + self.registers[b]
        if r > self.rsize:
            # Clamp
            r -= self.rsize
            self.registers[self.REG_C] = 1
        else:
            self.registers[self.REG_C] = 0

        self.registers[c] = r

    @opcode(0x02)
    def op_s(self, a, b, c):
        """Subtract b from a and store the result in c.

        mnemonic: s
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l or c > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        r = self.registers[a] - self.registers[b]
        if r < 0:
            r &= self.rsize
            self.registers[self.REG_C] = 1
        else:
            self.registers[self.REG_C] = 0

        self.registers[c] = r

    @opcode(0x03)
    def op_je(self, a, b, c):
        """Set cp to register c if a = b. Set rp to the instruction after.

        mnemonic: je
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l or c > l:
            self.registers[self.REG_E] = 1
            return

        if self.registers[a] == self.registers[b]:
            self.jump(self.registers[c])

    @opcode(0x04)
    def op_jn(self, a, b, c):
        """Set cp to register c if a != b. Set rp to the instruction after.

        mnemonic: jn
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l or c > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        if self.registers[a] != self.registers[b]:
            self.jump(self.registers[c])

    @opcode(0x05)
    def op_jg(self, a, b, c):
        """Set cp to register c if a > b. Set rp to the instruction after.

        mnemonic: jg
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l or c > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        if self.registers[a] > self.registers[b]:
            self.jump(self.registers[c])

    @opcode(0x06)
    def op_jl(self, a, b, c):
        """Set cp to register c if a < b. Set rp to the instruction after.

        mnemonic: jl
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l or c > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        if self.registers[a] < self.registers[b]:
            self.jump(self.registers[c])

    @opcode(0x07)
    def op_mc(self, a, b, c):
        """Move constant a into register b.

        mnemonic: mc
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if b > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        self.registers[b] = a

    @opcode(0x08)
    def op_mr(self, a, b, c):
        """Move register a into register b.

        mnemonic: mr
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        self.registers[b] = self.registers[a]

    @opcode(0x09)
    def op_mmr(self, a, b, c):
        """Move from memory location at register a into register b.

        mnemonic: mmr
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        l = len(self.memory) - 1
        if self.registers[a] > l:
            self.maybe_fault(self.Traps.MEM)
            self.registers[self.REG_E] = 1
            return

        self.registers[b] = self.memory[self.registers[a]]

    @opcode(0x0a)
    def op_mrm(self, a, b, c):
        """Move from register a into memory location at register b.

        mnemonic: mrm
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        l = len(self.memory) - 1
        if self.registers[b] > l:
            self.maybe_fault(self.Traps.MEM)
            self.registers[self.REG_E] = 1
            return

        self.memory[self.registers[b]] = self.registers[a]

    @opcode(0x0b)
    def op_a(self, a, b, c):
        """AND a and b together and place result in c.

        mnemonic: a
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l or c > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        self.registers[c] = self.registers[a] & self.registers[b]

    @opcode(0x0c)
    def op_o(self, a, b, c):
        """OR a and b together and place result in c.

        mnemonic: o
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l or c > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        self.registers[c] = self.registers[a] | self.registers[b]

    @opcode(0x0d)
    def op_x(self, a, b, c):
        """XOR a and b together and place result in c.

        mnemonic: x
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l or c > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        self.registers[c] = self.registers[a] ^ self.registers[b]

    @opcode(0x0e)
    def op_f(self, a, b, c):
        """Negate a and place result in b.

        mnemonic: f (flip)
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        self.registers[b] = ~self.registers[a]

    @opcode(0x0f)
    def op_sr(self, a, b, c):
        """Bit shift right a by register b amount and place result in c.

        mnemonic: sr
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l or c > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        self.registers[c] = ((self.registers[a] >> self.registers[b]) &
                             self.rsize)

    @opcode(0x10)
    def op_sl(self, a, b, c):
        """Bit shift left a by register b amount and place result in c.

        mnemonic: sl
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l or c > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        self.registers[c] = self.registers[a] >> self.registers[b]

    @opcode(0x11)
    def op_m(self, a, b, c):
        """Multiply a and b together and place result in c.

        mnemonic: m
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l or c > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        r = self.registers[a] * self.registers[b]
        if r > self.rsize:
            self.registers[self.REG_C] = 1
            r &= self.rsize
        else:
            self.registers[self.REG_C] = 0

        self.registers[c] = r

    @opcode(0x12)
    def op_d(self, a, b, c):
        """Nearest divide a and b together and place result in c.

        mnemonic: d
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l or c > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        if self.registers[b] == 0:
            # Divide by zero
            self.maybe_fault(self.Traps.DVZ)
            self.registers[self.REG_E] = 1
            return

        r = round(self.registers[a] / self.registers[b])
        self.registers[c] = r

    @opcode(0x13)
    def op_i(self, a, b, c):
        """Convert I/O region offset at register a and store it in b.

        mnemonic: i
        """
        self.registers[self.REG_E] = 0

        l = len(self.registers) - 1
        if a > l or b > l:
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        # Obtain the I/O offset
        off = self.memory.bus_start + self.registers[a]  # Real memory
        if off >= len(self.memory):  # Total address space
            self.maybe_fault(self.Traps.INV)
            self.registers[self.REG_E] = 1
            return

        self.registers[b] = off

    def memory_get(self, address):
        """Get memory at given address."""
        warn("This function is deprecated. "
             "Use direct accesses to self.memory instead.",
             DeprecationWarning)
        return self.memory[address]

    def memory_set(self, address, value):
        """Set memory at given address."""
        warn("This function is deprecated. "
             "Use direct accesses to self.memory instead.",
             DeprecationWarning)
        self.memory[address] = value

    def dispatch_opcode(self, opcode, a, b, c):
        """Dispatch an opcode."""
        fn = self.opcodes.get(opcode, None)
        if fn is None:
            self.maybe_fault(self.Traps.ILL)
            return
        fn(a, b, c)

    def execute_step(self):
        """Execute a single instruction."""
        cp = self.registers[self.REG_CP]
        args = tuple(self.memory[address] for address in
                     range(cp, cp + self.cells_per_reg))
        self.dispatch_opcode(*args)
        if cp == self.registers[self.REG_CP]:
            self.registers[self.REG_CP] += self.cells_per_reg

    def execute(self, steps=-1):
        """Execute until steps is reached."""
        i = 0
        while i != steps:
            self.execute_step()
            i += 1

            self.sched.run(False)

    def interrupt(self):
        """Signal an interrupt has been fired."""
        self.maybe_trap(self.Traps.INT)


if __name__ == "__main__":
    if not exists(argv[1]):
        print("No such image.")
        quit(1)

    with closing(MemoryImage.open_mmap(argv[1])) as image:
        m = Machine(image, MemoryMap, DefaultHardwareBus)
        m.execute()
