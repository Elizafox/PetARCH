#!/usr/bin/env python3

# Basic image loading library for Petarch.
# Licensed under the WTFPL.

from mmap import mmap
from os import remove, fsync, stat
from os.path import exists
from collections.abc import MutableSequence
from itertools import zip_longest


class MemoryImage(MutableSequence):
    """A memory image is what it says on the tin. The format may be either big
    or little endian, but defaults to big. The default size of each cell is 8
    bytes (don't make it less than that)."""

    CELL_SIZE = 8
    ENDIAN = "big"

    def __init__(self, memory, size=-1):
        self.memory = memory

        if size <= 0:
            self.size = len(memory)
        else:
            self.size = size

        self._fhandle = None

    @classmethod
    def open_mmap(cls, file, size=-1):
        """Open a memory image from disk; use mmap to synchronise it.

        :param file:
            The file to open.

        :param size:
            The size of the image. Existing images will be expanded or
            truncated to the new size, or left as-is if size is <= 0. If a new
            image is being created, then size must not be <= 0.
        """
        fhandle, size = cls._open(cls, file, size)
        memory = mmap(fhandle.fileno(), size)
        
        ret = cls(memory, size)
        ret._fhandle = fhandle
        return ret

    @classmethod
    def open_file(cls, file, size=-1):
        """Open a file once for reading the image; keep the fd open for sync.
        
        :param file:
            The file to open.

        :param size:
            The size of the image. Existing images will be expanded or
            truncated to the new size, or left as-is if size is <= 0. If a new
            image is being created, then size must not be <= 0.
        """
        fhandle, size = cls._open(cls, file, size)
        memory = bytearray(fhandle.read())
        fhandle.seek(0)

        ret = cls(memory, size)
        ret._fhandle = fhandle
        return ret

    @classmethod
    def write_file(cls, file, data):
        """Write data to the given file.

        :param file:
            The file to open.

        :param data:
            The data sequence of integers to write.

        :returns:
            Nothing.
        """
        if exists(file):
            # Clobber
            remove(file)

        memory = cls.open_file(file, len(data))
        for mi, d in enumerate(data):
            if not isinstance(d, int):
                d = int.from_bytes(d, self.ENDIAN)

            memory[mi] = d

        memory.close()

    open = open_mmap
    """The default."""

    def sync(self):
        """Synchronise the disk image."""
        if hasattr(self.memory, "flush"):
            self.memory.flush()
        else:
            # Manually synchronise
            self._fhandle.truncate(self.size)
            self._fhandle.write(self.memory)

    def close(self):
        """Close the file."""
        self.sync()

        if hasattr(self.memory, "close"):
            self.memory.close()

        if self._fhandle is not None:
            self._fhandle.close()

    @staticmethod
    def _open(cls, file, size):
        # Best to leave this alone if you don't know what you're doing.
        if size > 0:
            # Align to cell boundaries
            alignment = size % cls.CELL_SIZE
            if alignment != 0:
                size += cls.CELL_SIZE - alignment

        if exists(file):
            fhandle = open(file, "r+b")
            fsize = stat(fhandle.fileno()).st_size

            if size <= 0:
                # Check existing alignment of the file
                alignment = fsize % cls.CELL_SIZE
                if alignment != 0:
                    # Align current size
                    size = fsize + (cls.CELL_SIZE - alignment)
                else:
                    size = fsize
        else:
            if size <= 0:
                raise ValueError("Size must be specified for creation.")

            fhandle = open(file, "w+b")

        fhandle.truncate(size)
        fhandle.flush()
        return fhandle, size

    def __getitem__(self, item):
        m = self.memory
        if isinstance(item, slice):
            indices = item.indices(len(self))
            return [self[mi] for mi in range(*indices)]
        else:
            i = item * self.CELL_SIZE
            return int.from_bytes(m[i:i+self.CELL_SIZE], self.ENDIAN)

    def __setitem__(self, item, value):
        m = self.memory
        if isinstance(item, slice):
            indices = item.indices(len(self))
            len_s = (indices[1] - indices[0]) // indices[2]
            len_v = len(value)

            if len_s > len_v and indices[2] != 1:
                e = ("attempt to assign sequence of size {} to extended "
                     "slice of size {}").format(len_v, len_s)
                raise ValueError(e)

            for (vi, v), mi in zip_longest(enumerate(value), range(*indices)):
                if mi is None:
                    mi = vi + indices[1]

                self[mi] = v
        else:
            mi = item * self.CELL_SIZE
            m[mi:mi+self.CELL_SIZE] = int.to_bytes(value, self.CELL_SIZE,
                                                   self.ENDIAN)

    def __delitem__(self, item):
        raise NotImplementedError

    def __len__(self):
        return self.size // self.CELL_SIZE

    def insert(self, item, value):
        raise NotImplementedError

    def __contains__(self, item):
        m = self.memory
        for i in range(0, self.size, self.CELL_SIZE):
            if item == int.from_bytes(m[i:i+self.CELL_SIZE], self.ENDIAN):
                return True

        return False

    def __iter__(self):
        for mi in range(0, len(self)):
            yield self[mi]
