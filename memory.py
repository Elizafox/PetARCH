# Memory routines for PetARCH
# Licensed under the WTFPLv2

from itertools import zip_longest


class MemoryMap:
    """Memory map for the virtual machine, including the fault handler table.

    All MMIO is mapped "above" the real address space.
    """

    def __init__(self, image, machine, bus, realmem_size=0):
        """Initialise our memory map.

        args:
        image - our image, assumed to be a MemoryImage
        machine - our machine instance
        bus - our hardware bus mapping, as a class
        realmem_size - additional memory to allocate in the map, default is
                       none.
        """
        self.bits = machine.bits  # Assumes already initalised
        self.max_address = machine.rsize

        # Sizes calculated based on interrupt table + real memory
        # Hardware is excluded
        self.image_size = len(image)
        self.realmem_size = realmem_size
        self.total_size = self.image_size + self.realmem_size + 0x100

        # Start of the bus address space
        self.bus = bus(self, machine)
        self.bus_start = self.max_address - len(self.bus)

        # Initalise real memory
        self.memory = [0] * self.total_size

        # Copy the image
        image_start = 0x100
        image_end = image_start + (len(image) - 1)
        self.memory[image_start:image_end] = image[:]

        self.image = image

    def __getitem__(self, item):
        if isinstance(item, slice):
            indices = item.indices(len(self))
            return [self[i] for i in range(*indices)]

        if item <= self.total_size:
            return self.memory[item]
        
        # Wrap accesses
        item &= self.max_address

        if item >= self.bus_start:
            item -= self.bus_start
            return self.bus[item]

        raise Exception("Invalid memory access")

    def __setitem__(self, item, value):
        if isinstance(item, slice):
            indices = item.indices(len(self))

            len_s = (indices[1] - indices[0]) // indices[2]
            len_v = len(value)

            if len_s > len_v and indices[2] != 1:
                e = ("attempt to assign sequence of size {} to extended "
                     "slice of size {}").format(len_v, len_s)
                raise ValueError(e)

            for (vi, v), i in zip_longest(enumerate(value), range(*indices)):
                if i is None:
                    i = vi + indices[1]

                self[i] = v

        # Wrap accesses
        item &= self.max_address

        # Wrap values
        value &= (1 << self.image.CELL_SIZE) - 1

        if item <= self.total_size:
            self.memory[item] = value

        if item >= self.bus_start:
            item -= self.bus_start
            self.bus[item] = value

    def __delitem__(self, item):
        raise NotImplementedError

    def __len__(self):
        return self.max_address
